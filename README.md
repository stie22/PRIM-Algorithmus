# Thema PRIM Algorithmus

## Team 
Dennis James, Shabira Taie, Eugenia Laptev

## Funktionsbeschreibung
 Der Algorithmus von Prim dient der Berechnung eines minimalen Spannbaumes in einem zusammenhängenden, ungerichteten, kantengewichteten Graphen.

## Bezug zu Mathematik
Graphentheorie

## Zeitplan
- Grobkonzept Anfang November
- Feinkonzept Ende November
- Prototyp für Test Ende März

## Dokumentation
- Basierend auf Javadoc
- Basierend auf MS Powerpoint
- Sprache Deutsch
